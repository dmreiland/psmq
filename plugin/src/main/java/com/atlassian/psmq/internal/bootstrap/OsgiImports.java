package com.atlassian.psmq.internal.bootstrap;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.rdbms.TransactionalExecutorFactory;

/**
 * Our OSGi imports
 */
@Scanned
public class OsgiImports {
    @ComponentImport
    TransactionalExecutorFactory executorFactory;
}
