package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;
import org.apache.commons.lang.StringUtils;

import java.util.Optional;

import static com.atlassian.psmq.api.internal.Validations.checkNotNull;

/**
 * This allows you to build out {@link com.atlassian.psmq.api.message.QBuffer} objects
 */
@PublicApi
public class QBufferBuilder implements Appendable
{

    private final StringBuilder sb = new StringBuilder();
    private QMessageBuilder messageBuilder = null;

    QBufferBuilder() {
    }

    QBufferBuilder(final QMessageBuilder messageBuilder) {
        this.messageBuilder = messageBuilder;
    }

    /**
     * @return a builder that has no backing {@link com.atlassian.psmq.api.message.QMessageBuilder}
     */
    public static QBufferBuilder newBuffer()
    {
        return new QBufferBuilder();
    }

    /**
     * @return a builder that has a backing {@link com.atlassian.psmq.api.message.QMessageBuilder}
     */
    public static QBufferBuilder newBuffer(QMessageBuilder messageBuilder)
    {
        return new QBufferBuilder(messageBuilder);
    }

    @Override
    public QBufferBuilder append(final char c)
    {
        sb.append(c);
        return this;
    }

    @Override
    public QBufferBuilder append(final CharSequence csq)
    {
        sb.append(csq);
        return this;
    }

    public QBufferBuilder append(final boolean b)
    {
        sb.append(b);
        return this;
    }

    public QBufferBuilder append(final char[] str)
    {
        sb.append(str);
        return this;
    }

    public QBufferBuilder append(final char[] str, final int offset, final int len)
    {
        sb.append(str, offset, len);
        return this;
    }

    public QBufferBuilder append(final double d)
    {
        sb.append(d);
        return this;
    }

    public QBufferBuilder appendCodePoint(final int codePoint)
    {
        sb.appendCodePoint(codePoint);
        return this;
    }

    public QBufferBuilder append(final float f)
    {
        sb.append(f);
        return this;
    }

    public QBufferBuilder append(final int i)
    {
        sb.append(i);
        return this;
    }

    public QBufferBuilder append(final long lng)
    {
        sb.append(lng);
        return this;
    }

    public QBufferBuilder append(final Object obj)
    {
        sb.append(obj);
        return this;
    }

    public QBufferBuilder append(final String str)
    {
        sb.append(str);
        return this;
    }

    public QBufferBuilder append(final StringBuffer sb)
    {
        this.sb.append(sb);
        return this;
    }

    @Override
    public QBufferBuilder append(final CharSequence csq, final int start, final int end)
    {
        sb.append(csq, start, end);
        return this;
    }

    public <T extends CharSequence> QBufferBuilder append(final Optional<T> option)
    {
        if (option.isPresent())
        {
            sb.append(option.get());
        }
        return this;
    }

    public QBuffer build()
    {
        QBufferImpl qBuffer = new QBufferImpl(sb.toString());
        if (messageBuilder != null)
        {
            messageBuilder.withBuffer(qBuffer);
        }
        return qBuffer;
    }

    public QMessageBuilder buildBuffer()
    {
        checkNotNull(messageBuilder, "The builder must be created with a message builder");

        QBufferImpl qBuffer = new QBufferImpl(sb.toString());
        messageBuilder.withBuffer(qBuffer);
        return messageBuilder;
    }


    static class QBufferImpl implements QBuffer
    {
        private final String innards;

        QBufferImpl(final String innards)
        {
            this.innards = innards;
        }


        @Override
        public long length()
        {
            return innards.length();
        }

        @Override
        public String asString()
        {
            return innards;
        }

        @Override
        public String toString()
        {
            return "len:" + innards.length() + ":" + StringUtils.substring(innards, 0, 100);
        }
    }

}
