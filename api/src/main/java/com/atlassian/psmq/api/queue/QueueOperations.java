package com.atlassian.psmq.api.queue;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.QException;
import com.atlassian.psmq.api.paging.PageResponse;

import java.util.Optional;

/**
 * The set of operations that can be performed against queues in an administration sense
 *
 * @since v1.0
 */
@PublicApi
public interface QueueOperations {
    /**
     * This will produce a paged response of queues based on rhe query
     *
     * @param query the query to execute
     *
     * @return a page of matching queues
     */
    PageResponse<Queue> queryQueues(final QueueQuery query);

    /**
     * This will get the queue by id.
     *
     * @param queueId the intended queue by id
     *
     * @return the queue
     *
     * @throws QException in exceptional circumstances
     */
    Queue getQueue(QId queueId) throws QException;

    /**
     * This will update a queue.
     *
     * @param queue       the queue to update
     * @param queueUpdate the update to be made
     *
     * @return the updated Queue
     *
     * @throws QException in exceptional circumstances
     */
    Queue updateQueue(Queue queue, QueueUpdate queueUpdate) throws QException;

    /**
     * This will access the queue as defined, creating the queue if its does not already exist or returning it by name then it will be
     * returned and this operation becomes read only
     *
     * @param queueDefinition the intended queue definition
     *
     * @return the queue
     *
     * @throws QException in exceptional circumstances
     */
    Queue accessQueue(QueueDefinition queueDefinition) throws QException;

    /**
     * This will try to exclusively access the queue as defined, creating the queue if its does not already exist, via this
     * {@link com.atlassian.psmq.api.QSession}'s claimant information.
     *
     * @param queueDefinition the intended queue definition
     *
     * @return the queue asn option or none() if the queue cannot be exclusively accessed
     *
     * @throws QException in exceptional circumstances
     */
    Optional<Queue> exclusiveAccessQueue(QueueDefinition queueDefinition) throws QException;

    /**
     * This will heart beat a queue that was previously exclusively accessed using this {@link com.atlassian.psmq.api.QSession}'s
     * claimant information.  If you don't do this within the sessions define heart beat time then its possible
     * for some other process to gain exclusive access to this queue because the system will think you have died.
     *
     * @param queue the queue to heart beat
     *
     * @throws QException in exceptional circumstances
     */
    void heartBeatQueue(Queue queue) throws QException;

    /**
     * This will release a queue that was previously exclusively accessed using this {@link com.atlassian.psmq.api.QSession}'s
     * claimant information.
     *
     * @param queue the queue to release
     *
     * @throws QException in exceptional circumstances
     */
    void releaseQueue(Queue queue) throws QException;

    /**
     * Releases a queue that was previously exclusively accessed using this {@link com.atlassian.psmq.api.QSession}'s
     * claimant information, only if the queue is empty.
     *
     * <p> This method will return {@code true} if the queue is released successfully. If the queue still contains
     * messages, this method will return {@code false}, and the queue will not be released.
     *
     * @param queue the queue to release
     *
     * @return true if the queue was released successfully, false if the queue could not be released because it still
     * contains some messages.
     *
     * @throws QException in exceptional circumstances
     */
    boolean releaseQueueIfEmpty(Queue queue) throws QException;

    /**
     * This will release all queue that was previously exclusively accessed using this {@link com.atlassian.psmq.api.QSession}'s
     * claimant information.
     *
     * @throws QException in exceptional circumstances
     */
    void releaseAllQueues() throws QException;

    /**
     * This will un-resolve all claimed messages in the queue that are associated with the {@link com.atlassian.psmq.api.QSession}'s claimant
     * information, which puts them back into a state ready for a {@link com.atlassian.psmq.api.message.QMessageConsumer} to process them.
     *
     * @param queue the queue to purge messages from
     *
     * @throws QException in exceptional circumstances
     */
    void unresolveAllClaimedMessages(Queue queue) throws QException;

    /**
     * This will un-resolve all claimed messages, across any queue, that are associated with the {@link com.atlassian.psmq.api.QSession}'s claimant
     * information, which puts them back into a state ready for a {@link com.atlassian.psmq.api.message.QMessageConsumer} to
     * process them.
     *
     * @throws QException in exceptional circumstances
     */
    void unresolveAllClaimedMessages() throws QException;

    /**
     * This will delete all messages in the queue but leave the queue in place
     *
     * @param queue the queue to purge messages from
     *
     * @throws QException in exceptional circumstances
     */
    void purgeQueue(Queue queue) throws QException;

    /**
     * This will delete all messages in the queue and then delete the queue itself
     *
     * @param queue the queue to delete
     *
     * @throws QException in exceptional circumstances
     */
    void deleteQueue(Queue queue) throws QException;

    /**
     * This gives back a queue browser that you can use to peek inside the queue
     *
     * @param queue the queue to browser
     *
     * @return a browser of that queue
     *
     * @throws QException in exceptional circumstances
     */
    QueueBrowser browser(Queue queue) throws QException;

}
