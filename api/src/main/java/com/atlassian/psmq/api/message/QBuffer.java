package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;

/**
 * A buffer is the data that goes into a {@link com.atlassian.psmq.api.message.QMessage}
 *
 * @since v0.x
 */
@PublicApi
public interface QBuffer
{
    /**
     * @return the length of the buffer
     */
    long length();

    /**
     * @return the current buffer as a string
     */
    String asString();
}
