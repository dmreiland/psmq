package com.atlassian.psmq.api.property;

import com.atlassian.annotations.PublicApi;

import java.util.Date;
import java.util.Optional;
import java.util.Set;

/**
 * A set container of properties
 *
 * @since v1.0
 */
@PublicApi
public interface QPropertySet {

    /**
     * Returns an optional property under the specified name
     *
     * @param name the name of the property to find
     *
     * @return an optional property if its present
     */
    Optional<QProperty> property(String name);

    /**
     * Returns an optional string property value under the specified name
     *
     * @param name the name of the property to find
     *
     * @return an optional value if its present of this type
     */
    Optional<String> stringValue(String name);

    /**
     * Returns an optional long property value under the specified name
     *
     * @param name the name of the property to find
     *
     * @return an optional value if its present of this type
     */
    Optional<Long> longValue(String name);

    /**
     * Returns an optional date property value under the specified name
     *
     * @param name the name of the property to find
     *
     * @return an optional value if its present of this type
     */
    Optional<Date> dateValue(String name);

    /**
     * Returns an optional boolean property value under the specified name
     *
     * @param name the name of the property to find
     *
     * @return an optional value if its present of this type
     */
    Optional<Boolean> booleanValue(String name);

    /**
     * @return the names of the properties in the set
     */
    Set<String> names();

    /**
     * @return the properties as a set
     */
    Set<QProperty> asSet();

}
