package com.atlassian.psmq.api;

import java.sql.Connection;

/**
 * If you use an {@link com.atlassian.psmq.api.QSessionDefinition.CommitStrategy#EXTERNAL_COMMIT} or {@link com.atlassian.psmq.api.QSessionDefinition.CommitStrategy#SESSION_COMMIT} then you MUST
 * provide the connection yourself to the {@link com.atlassian.psmq.api.QSessionDefinition}.  If you want to take control
 * of the connections in general then you can use this interface
 *
 * @since v1.0
 */
public interface QConnectionProvider
{
    /**
     * @return the connection to use in the QSession
     */
    Connection borrowConnection();

    /**
     * One the queuing system is finished with the connection it will return it thus.
     *
     * @param connection the connection to return
     */
    void returnConnection(Connection connection);
}
