package com.atlassian.psmq.api.queue;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.internal.Validations;
import com.atlassian.psmq.api.property.QProperty;
import com.atlassian.psmq.api.property.QPropertyBuilder;
import com.atlassian.psmq.api.property.QPropertySet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static com.atlassian.psmq.api.internal.Validations.checkNotNull;

/**
 * A builder of {@link com.atlassian.psmq.api.queue.QueueDefinition}s
 *
 * @since v1.0
 */
@PublicApi
public class QueueDefinitionBuilder {
    private String name;
    private String purpose;
    private Optional<QTopic> topic;
    private Set<QProperty> properties;

    public QueueDefinitionBuilder() {
        this.name = "q_" + UUID.randomUUID().toString();
        this.purpose = String.format("A queue defined on '%s'", new SimpleDateFormat("yyyy-mm-dd.hh.MM").format(new Date()));
        this.topic = Optional.empty();
        this.properties = new HashSet<>();
    }

    public static QueueDefinitionBuilder newDefinition() {
        return new QueueDefinitionBuilder();
    }

    /**
     * Sets the name of the queue
     *
     * @param name the queue name
     *
     * @return this builder
     */
    public QueueDefinitionBuilder withName(String name) {
        this.name = Validations.checkQueueName(name);
        return this;
    }

    /**
     * Sets the purpose of the queue
     *
     * @param purpose the queue purpose
     *
     * @return this builder
     */
    public QueueDefinitionBuilder withPurpose(String purpose) {
        this.purpose = Validations.checkQueuePurpose(purpose);
        return this;
    }

    /**
     * Sets the topic of the queue
     *
     * @param topic the queue topic
     *
     * @return this builder
     */
    public QueueDefinitionBuilder withTopic(QTopic topic) {
        this.topic = Optional.of(checkNotNull(topic));
        return this;
    }

    public QueueDefinitionBuilder withProperty(String name, String value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    public QueueDefinitionBuilder withProperty(String name, long value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    public QueueDefinitionBuilder withProperty(String name, boolean value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    public QueueDefinitionBuilder withProperty(String name, Date value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    public QueueDefinitionBuilder withProperty(QProperty property) {
        this.properties.add(checkNotNull(property));
        return this;
    }

    public QueueDefinitionBuilder withProperties(Set<QProperty> properties) {
        for (QProperty property : checkNotNull(properties)) {
            withProperty(property);
        }
        return this;
    }

    public QueueDefinitionBuilder withProperties(QPropertySet properties) {
        for (QProperty property : checkNotNull(properties).asSet()) {
            withProperty(property);
        }
        return this;
    }

    private QPropertyBuilder prop() {
        return QPropertyBuilder.newProperty();
    }


    public QueueDefinition build() {
        return new QueueDefinition() {
            @Override
            public String name() {
                return name;
            }

            @Override
            public String purpose() {
                return purpose;
            }

            @Override
            public Optional<QTopic> topic() {
                return topic;
            }

            @Override
            public Set<QProperty> properties() {
                return properties;
            }
        };
    }
}
