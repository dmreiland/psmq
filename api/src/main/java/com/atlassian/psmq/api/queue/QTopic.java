package com.atlassian.psmq.api.queue;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.internal.Validations;

/**
 * {@link Queue}s can have a topics associated with them
 *
 * @since v1.0
 */
@PublicApi
public class QTopic
{
    /**
     * Returns a new topic form the specified string
     *
     * @param topic the string topic
     * @return a new topic
     *
     */
    public static QTopic newTopic(String topic)
    {
        return new QTopic(topic);
    }

    private final String value;

    public QTopic(final String value)
    {
        this.value = Validations.checkQTopic(value);
    }

    /**
     * @return the content type value
     */
    public String value()
    {
        return value;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        final QTopic that = (QTopic) o;

        if (!value.equals(that.value))
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        return value.hashCode();
    }
}
