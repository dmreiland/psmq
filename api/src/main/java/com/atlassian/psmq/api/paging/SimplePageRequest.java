package com.atlassian.psmq.api.paging;

import com.atlassian.annotations.PublicApi;
import com.google.common.base.Objects;

/**
 * A simple immutable implementation of PageRequest
 */
@PublicApi
public class SimplePageRequest implements PageRequest
{
    public static PageRequest ONE = new SimplePageRequest(0,1);

    private final int start;
    private final int limit;

    public SimplePageRequest(LimitedPageRequest request)
    {
        this(request.getStart(), request.getLimit());
    }

    public SimplePageRequest(int start, int limit)
    {
        this.start = start;
        this.limit = limit;
    }

    @Override
    public int getLimit()
    {
        return limit;
    }

    @Override
    public int getStart()
    {
        return start;
    }

    public String toString()
    {
        return Objects.toStringHelper(this).add("start", start).add("limit", limit).toString();
    }
}
