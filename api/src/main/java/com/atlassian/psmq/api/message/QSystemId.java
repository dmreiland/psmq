package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;

/**
 * This represents an unique identifier that is assigned to a message once its placed into queue. The caller is not
 * expected to ever set this value
 *
 * @since v1.0
 */
@PublicApi
public class QSystemId
{
    private final long value;

    public QSystemId(final long value)
    {
        this.value = value;
    }


    /**
     * @return the value of the system id
     */
    public long value()
    {
        return value;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        final QSystemId that = (QSystemId) o;

        return value == that.value;
    }

    @Override
    public int hashCode()
    {
        return Long.valueOf(value).hashCode();
    }

    @Override
    public String toString()
    {
        return String.valueOf(value);
    }
}
