package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.internal.Validations;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * This represents a specific messageId of a {@link com.atlassian.psmq.api.message.QMessage} that the consumer
 * of the API can set.  This allows you to set your own "specific" identifier into a message
 *
 * @since v1.0
 */
@PublicApi
public class QMessageId
{
    private final String value;

    /**
     * Returns a mesage id with the specified value
     * @param idString the id to use
     * @return a QMessageId
     */
    public static QMessageId id(String idString) {
        return new QMessageId(idString);
    }

    public QMessageId(final String value)
    {
        this.value = Validations.checkQMessageId(value);
    }

    public QMessageId()
    {
        this.value = computeMsgId();
    }

    /**
     * @return the value of the message id
     */
    public String value()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return value;
    }

    private String computeMsgId()
    {
        String time = new SimpleDateFormat("YYYYMMDDHHmmssSSSz").format(new Date());
        String uuid = UUID.randomUUID().toString();
        return time + "-" + uuid;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        final QMessageId that = (QMessageId) o;

        return value.equals(that.value);
    }

    @Override
    public int hashCode()
    {
        return value.hashCode();
    }
}
