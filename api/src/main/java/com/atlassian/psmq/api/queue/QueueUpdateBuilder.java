package com.atlassian.psmq.api.queue;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.property.QProperty;
import com.atlassian.psmq.api.property.QPropertyBuilder;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.psmq.api.internal.Validations.checkNotNull;
import static com.atlassian.psmq.api.internal.Validations.checkQueuePurpose;

/**
 * A builder of {@link com.atlassian.psmq.api.queue.QueueUpdate}s
 *
 * @since v1.0
 */
@PublicApi
public class QueueUpdateBuilder {
    private Optional<String> purpose = Optional.empty();
    private Optional<QTopic> topic = Optional.empty();
    private Set<QProperty> properties = new HashSet<>();

    public QueueUpdateBuilder() {
    }

    public static QueueUpdateBuilder newUpdate() {
        return new QueueUpdateBuilder();
    }

    /**
     * Sets the purpose of the queue
     *
     * @param purpose the queue purpose
     *
     * @return this builder
     */
    public QueueUpdateBuilder withPurpose(String purpose) {
        this.purpose = Optional.of(checkQueuePurpose(purpose));
        return this;
    }

    /**
     * Sets the topic of the queue
     *
     * @param topic the queue topic
     *
     * @return this builder
     */
    public QueueUpdateBuilder withTopic(QTopic topic) {
        this.topic = Optional.of(checkNotNull(topic));
        return this;
    }

    /**
     * Sets a string property value on the queue.
     *
     * @param name  the new property name
     * @param value the new property value
     *
     * @return this builder
     */
    public QueueUpdateBuilder withProperty(String name, String value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    /**
     * Sets a number property value on the queue.
     *
     * @param name  the new property name
     * @param value the new property value
     *
     * @return this builder
     */
    public QueueUpdateBuilder withProperty(String name, long value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    /**
     * Sets a boolean property value on the queue.
     *
     * @param name  the new property name
     * @param value the new property value
     *
     * @return this builder
     */
    public QueueUpdateBuilder withProperty(String name, boolean value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    /**
     * Sets a date property value on the queue.
     *
     * @param name  the new property name
     * @param value the new property value
     *
     * @return this builder
     */
    public QueueUpdateBuilder withProperty(String name, Date value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    /**
     * Sets a property on the queue.
     *
     * @param property the new property to set
     *
     * @return this builder
     */
    public QueueUpdateBuilder withProperty(QProperty property) {
        this.properties.add(checkNotNull(property));
        return this;
    }

    /**
     * Sets a new properties on the queue.
     *
     * @param properties the new properties
     *
     * @return this builder
     */
    public QueueUpdateBuilder withProperties(Set<QProperty> properties) {
        for (QProperty property : checkNotNull(properties)) {
            withProperty(property);
        }
        return this;
    }

    private QPropertyBuilder prop() {
        return QPropertyBuilder.newProperty();
    }

    /**
     * @return the built {@link QueueUpdate}
     */
    public QueueUpdate build() {
        return new QueueUpdate() {

            @Override
            public Optional<String> purpose() {
                return purpose;
            }

            @Override
            public Optional<QTopic> topic() {
                return topic;
            }

            @Override
            public Optional<Set<QProperty>> properties() {
                return properties.isEmpty() ? Optional.empty() : Optional.of(properties);
            }
        };
    }
}
