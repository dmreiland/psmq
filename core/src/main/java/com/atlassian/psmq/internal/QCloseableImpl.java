package com.atlassian.psmq.internal;

import com.atlassian.psmq.api.QException;

import java.io.Closeable;

/**
 * A base class for consumers and producers that provides closed-ness
 */
public class QCloseableImpl implements AutoCloseable
{
    private volatile boolean closed = false;

    public QCloseableImpl()
    {
    }


    protected void checkNotClosed(String msg) throws QException
    {
        if (closed)
        {
            throw new QException(msg);
        }
    }

    public boolean isClosed()
    {
        return closed;
    }

    @Override
    public void close()
    {
        closed = true;
    }
}
