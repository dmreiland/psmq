package com.atlassian.psmq.internal.queue;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.property.QProperty;
import com.atlassian.psmq.api.property.QPropertySet;
import com.atlassian.psmq.api.property.QPropertySetBuilder;
import com.atlassian.psmq.api.queue.QId;
import com.atlassian.psmq.api.queue.QTopic;
import com.atlassian.psmq.api.queue.Queue;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.Date;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.psmq.api.internal.Validations.checkNotNull;
import static com.atlassian.psmq.internal.util.HelperKit.toOptional;

/**
 */
public class QueueImpl implements Queue
{
    private final QId queueId;
    private final String name;
    private final String purpose;
    private final Date created;
    private final Date lastModified;
    private final long approxMsgCount;
    private final Option<QTopic> topic;
    private final QPropertySet properties;

    public QueueImpl(final long queueId, final String name, final String purpose, final Option<QTopic> topic, final Date created, final Date lastModified, final long approxMsgCount, final Set<QProperty> properties)
    {
        this.queueId = new QId(queueId);
        this.purpose = purpose;
        this.topic = topic;
        this.created = created;
        this.lastModified = lastModified;
        this.approxMsgCount = approxMsgCount;
        this.name = checkNotNull(name);
        this.properties = QPropertySetBuilder.newPropertySet(properties);
    }

    public QId id()
    {
        return queueId;
    }

    @Override
    public String name()
    {
        return name;
    }

    public String purpose()
    {
        return purpose;
    }

    @Override
    public Optional<QTopic> topic()
    {
        return toOptional(topic);
    }

    @Override
    public long approxMsgCount()
    {
        return approxMsgCount;
    }

    @Override
    public Date created()
    {
        return created;
    }

    @Override
    public Date lastModified()
    {
        return lastModified;
    }

    @Override
    public QPropertySet properties()
    {
        return properties;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this).
                append("queueId", queueId).
                append("name", name).
                append("approxMsgCount", approxMsgCount).
                toString();
    }
}
