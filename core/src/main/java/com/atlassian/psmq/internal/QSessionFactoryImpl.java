package com.atlassian.psmq.internal;

import com.atlassian.fugue.Option;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnectionConverter;
import com.atlassian.psmq.api.QConnectionProvider;
import com.atlassian.psmq.api.QException;
import com.atlassian.psmq.api.QSession;
import com.atlassian.psmq.api.QSessionDefinition;
import com.atlassian.psmq.api.QSessionFactory;
import com.atlassian.psmq.api.internal.Validations;
import com.atlassian.psmq.internal.io.MessageReader;
import com.atlassian.psmq.internal.io.MessageWriter;
import com.atlassian.psmq.internal.queue.QueueOperationsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.atlassian.psmq.api.QSessionDefinition.CommitStrategy.EXTERNAL_COMMIT;
import static com.atlassian.psmq.api.QSessionDefinition.CommitStrategy.SESSION_COMMIT;
import static com.atlassian.psmq.api.internal.Validations.checkNotNull;
import static com.atlassian.psmq.internal.util.HelperKit.toOption;

/**
 * Our top level session factory.  This is where is all begins
 */
@ExportAsService(QSessionFactory.class)
@Component
public class QSessionFactoryImpl implements QSessionFactory {
    private final DatabaseAccessor databaseAccessor;
    private final DatabaseConnectionConverter databaseConnectionConverter;
    private final QueueOperationsImpl queueOperations;
    private final MessageWriter messageWriter;
    private final MessageReader messageReader;

    @Autowired
    public QSessionFactoryImpl(DatabaseAccessor databaseAccessor, DatabaseConnectionConverter databaseConnectionConverter, final QueueOperationsImpl queueOperations, final MessageWriter messageWriter, final MessageReader messageReader) {
        this.databaseAccessor = databaseAccessor;
        this.databaseConnectionConverter = databaseConnectionConverter;
        this.queueOperations = queueOperations;
        this.messageWriter = messageWriter;
        this.messageReader = messageReader;
    }

    @Override
    public QSession createSession(final QSessionDefinition sessionDefinition) throws QException {
        return new QSessionImpl(databaseConnectionConverter, databaseAccessor, makeConnectionProvider(sessionDefinition), checkNotNull(sessionDefinition), queueOperations, messageWriter, messageReader);
    }

    private Option<QConnectionProvider> makeConnectionProvider(final QSessionDefinition sessionDefinition) {
        boolean noProvider = !sessionDefinition.connectionProvider().isPresent();
        if (noProvider) {
            QSessionDefinition.CommitStrategy commitStrategy = sessionDefinition.commitStrategy();
            if (commitStrategy == EXTERNAL_COMMIT) {
                Validations.checkState(false, "You MUST provide a QConnectionProvider if you use EXTERNAL commit strategy");
            }
            if (commitStrategy == SESSION_COMMIT) {
                Validations.checkState(false, "You MUST provide a QConnectionProvider if you use SESSION commit strategy");
            }
        }
        return toOption(sessionDefinition.connectionProvider());
    }
}
