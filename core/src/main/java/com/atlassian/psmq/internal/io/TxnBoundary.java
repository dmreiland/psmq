package com.atlassian.psmq.internal.io;

import com.google.common.base.Function;

/**
 * A Txn boundary is some code that needs to run against the database and have commits or rollback boundaries.
 */
public interface TxnBoundary
{
    <T> T run(String operationMessage, Function<TxnContext, T> txnFunction);
}
