package com.atlassian.psmq.internal.rest.implementation;

import com.atlassian.psmq.api.paging.PageRequest;
import com.atlassian.psmq.api.paging.SimplePageRequest;

import javax.ws.rs.core.UriInfo;

/**
 */
public class RestPageRequest
{
    public static PageRequest getPaging(final UriInfo uriInfo, int defaultLimit)
    {
        int start = getQueryParamInt(uriInfo,"start",0);
        int limit = getQueryParamInt(uriInfo,"limit",defaultLimit);
        return new SimplePageRequest(start,limit);
    }

    private static int getQueryParamInt(final UriInfo uriInfo, final String name, final int defaultValue)
    {
        String first = uriInfo.getQueryParameters().getFirst(name);
        try
        {
            return Integer.parseInt(first);
        }
        catch (NumberFormatException e)
        {
            return defaultValue;
        }
    }

}
