package com.atlassian.psmq.internal.rest.representations;

import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.internal.util.rest.RestKit;

import java.util.Collection;

/**
 */
@SuppressWarnings ({ "FieldCanBeLocal", "UnusedDeclaration" })
public class QueueDTO
{
    private final long id;
    private final String name;
    private final String purpose;
    private final String topic;
    private final String created;
    private final String lastModified;
    private final long approxMsgCount;
    private final Collection<PropertyDTO> properties;

    public QueueDTO(Queue q)
    {
        this.id = q.id().value();
        this.name = q.name();
        this.purpose = q.purpose();
        this.topic = q.topic().map(t -> t.value()).orElse(null);
        this.created = RestKit.isoDate(q.created());
        this.lastModified = RestKit.isoDate(q.lastModified());
        this.approxMsgCount = q.approxMsgCount();
        this.properties = PropertyDTO.of(q.properties());
    }
}
