package com.atlassian.psmq.backdoor.internal.tests;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.QConnectionProvider;
import com.atlassian.psmq.api.QException;
import com.atlassian.psmq.api.QSession;
import com.atlassian.psmq.api.QSessionDefinition;
import com.atlassian.psmq.api.QSessionDefinitionBuilder;
import com.atlassian.psmq.api.message.QClaimant;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageBuilder;
import com.atlassian.psmq.api.message.QMessageProducer;
import com.atlassian.psmq.api.property.QPropertySet;
import com.atlassian.psmq.api.queue.QTopic;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueDefinition;
import com.atlassian.psmq.api.queue.QueueDefinitionBuilder;
import com.atlassian.psmq.backdoor.internal.util.QComponentAccessor;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

/**
 * Helpers to allow better testing
 */
public class Fixtures {
    public static String rqName() {
        return "q_" + UUID.randomUUID().toString();
    }

    public static QueueDefinition qDefinition(final String qName) {
        return QueueDefinitionBuilder.newDefinition().withName(qName).build();
    }

    public static void writeNMessages(final int n, final QSession session, final Queue queue) {
        QMessageProducer producer = session.createProducer(queue);
        for (int i = 0; i < n; i++) {
            producer.writeMessage(basicMsg("Message " + i));
        }
    }

    public static QMessage basicMsg(final String s) {
        return QMessageBuilder.newMsg().newBuffer().append(s).buildBuffer().build();
    }

    public static QMessageBuilder buildMsg(final String s) {
        return QMessageBuilder.newMsg().newBuffer().append(s).buildBuffer();
    }

    public static Queue createQ(final QSession session, final Option<String> name, Option<QTopic> topic) {
        QueueDefinitionBuilder builder = QueueDefinitionBuilder.newDefinition();
        name.forEach(n -> builder.withName(n));
        topic.forEach(t -> builder.withTopic(t));
        return session.queueOperations().accessQueue(builder.build());
    }

    public static Queue createQ(final QSession session, final Option<String> name, QPropertySet properties) {
        QueueDefinitionBuilder builder = QueueDefinitionBuilder.newDefinition();
        name.forEach(n -> builder.withName(n));
        builder.withProperties(properties);
        return session.queueOperations().accessQueue(builder.build());
    }

    public static void eraseQ(final long queueId) {
        QComponentAccessor.getQueueResetter().eraseQ(queueId);
    }

    public static void eraseQs() {
        QComponentAccessor.getQueueResetter().eraseQs();
    }

    /**
     * Tests can fail and leave connections hanging.  This helps clean them up
     */
    private static class FixtureConnectionProvider implements QConnectionProvider {
        private final Connection connection;

        private FixtureConnectionProvider(Connection connection) {
            this.connection = connection;
        }

        @Override
        public Connection borrowConnection() {
            return connection;

        }

        @Override
        public void returnConnection(final Connection connection) {
            rollback(connection);
        }

        public void tearDown() {
            rollback(connection);
            close(connection);
        }

        private void close(final Connection connection) {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println("Exception closing connection during test :  " + e.toString());
            }
        }

        private void rollback(final Connection connection) {
            try {
                connection.rollback();
            } catch (Exception e) {
                System.out.println("Exception rolling back connection during test :  " + e.toString());
            }
        }
    }

    /**
     * A basic fixture for PSMQ testing
     */
    public static class Fixture {
        List<QSession> sessions = new ArrayList<>();

        private QSession track(final QSession session) {
            sessions.add(session);
            return session;
        }

        Runnable onClose() {
            return () -> System.out.println("\t\tSession closed");
        }

        public QSession autoCommitSession() {
            QSessionDefinition sessionDefinition = QSessionDefinitionBuilder.newDefinition().withAutoCommitStrategy().withOnClose(onClose()).build();
            return track(QComponentAccessor.getSessionFactory().createSession(sessionDefinition));
        }

        public QSession autoCommitSessionWithClaimant(QClaimant claimant, long heartBeat) {
            QSessionDefinition sessionDefinition = QSessionDefinitionBuilder.newDefinition().withAutoCommitStrategy().withClaimant(claimant, heartBeat).withOnClose(onClose()).build();
            return track(QComponentAccessor.getSessionFactory().createSession(sessionDefinition));
        }

        public QSession sessionCommitSession(Connection connection) {
            FixtureConnectionProvider connectionProvider = new FixtureConnectionProvider(connection);
            QSessionDefinition sessionDefinition = QSessionDefinitionBuilder.newDefinition().withSessionCommitStrategy(connectionProvider).withOnClose(onClose()).build();
            return track(QComponentAccessor.getSessionFactory().createSession(sessionDefinition));
        }

        public QSession externalCommitSession(Connection connection) {
            FixtureConnectionProvider connectionProvider = new FixtureConnectionProvider(connection);
            QSessionDefinition sessionDefinition = QSessionDefinitionBuilder.newDefinition().withExternalCommitStrategy(connectionProvider).withOnClose(onClose()).build();

            return track(QComponentAccessor.getSessionFactory().createSession(sessionDefinition));
        }

        public void tearDown() {
            // close each session
            sessions.forEach(s -> closeSession(s));
        }

        private void closeSession(final QSession qSession) {
            try {
                if (qSession != null) {
                    qSession.close();
                }
            } catch (QException e) {
                e.printStackTrace(System.out);
            }
        }

        public void withConnection(Consumer<Connection> f) {
            QComponentAccessor.getDatabaseAccessor().run(dbConn -> {
                f.accept(dbConn.getJdbcConnection());
                return true;
            });
        }
    }
}
