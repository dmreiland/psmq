package com.atlassian.psmq.backdoor.internal.tests;

import com.atlassian.psmq.api.QSession;
import ivmtestrunner.api.client.JunitClientSideRemoteTestRunner;
import ivmtestrunner.api.client.TestResource;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

/**
 */
@RunWith(JunitClientSideRemoteTestRunner.class)
@TestResource(url = "http://localhost:2990/jira/rest/psmqbackdoor/tests")
public class BaseApiTest
{
    protected QSession qSession;
    protected Fixtures.Fixture psmqFixture;
    protected long now;

    @Before
    public void setUp() throws Exception
    {
        Fixtures.eraseQs();
        psmqFixture = new Fixtures.Fixture();
        qSession = psmqFixture.autoCommitSession();
        now = System.currentTimeMillis();
    }

    @After
    public void tearDown() throws Exception
    {
        qSession.close();
        psmqFixture.tearDown();
        Fixtures.eraseQs();
    }

}
