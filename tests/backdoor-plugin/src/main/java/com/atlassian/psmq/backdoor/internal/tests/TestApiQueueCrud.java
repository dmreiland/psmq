package com.atlassian.psmq.backdoor.internal.tests;

import com.atlassian.psmq.api.property.QPropertySet;
import com.atlassian.psmq.api.property.QPropertySetBuilder;
import com.atlassian.psmq.api.queue.QTopic;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueDefinition;
import com.atlassian.psmq.api.queue.QueueDefinitionBuilder;
import com.atlassian.psmq.api.queue.QueueQuery;
import com.atlassian.psmq.api.queue.QueueQueryBuilder;
import com.atlassian.psmq.api.queue.QueueUpdate;
import com.atlassian.psmq.api.queue.QueueUpdateBuilder;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 */
public class TestApiQueueCrud extends BaseApiTest {

    @Test
    public void test_adding_queue() {
        // assemble
        String qName = Fixtures.rqName();
        QueueDefinition queueDef = QueueDefinitionBuilder.newDefinition()
                .withName(qName)
                .withPurpose("To save the world")
                .build();

        // act
        Queue queue = qSession.queueOperations().accessQueue(queueDef);

        // assert
        assertThat(queue, notNullValue());
        assertThat(queue.name(), equalTo(qName));
        assertThat(queue.purpose(), equalTo("To save the world"));
        assertThat(queue.topic().isPresent(), equalTo(false));
        assertThat(queue.approxMsgCount(), equalTo(0L));
        assertThat(queue.created().getTime() >= now, equalTo(true));
    }


    @Test
    public void test_queue_updating() throws Exception {
        // assemble
        String qName = Fixtures.rqName();
        QueueDefinition queueDef = QueueDefinitionBuilder.newDefinition()
                .withName(qName)
                .withPurpose("Initial Purpose")
                .withTopic(QTopic.newTopic("topic1"))
                .withProperty("key1", "value1")
                .withProperty("key2", "value2")
                .build();
        Queue queue = qSession.queueOperations().accessQueue(queueDef);

        // act
        QueueUpdate queueUpdate = QueueUpdateBuilder.newUpdate()
                .withTopic(QTopic.newTopic("newTopic"))
                .withPurpose("New Purpose")
                .withProperty("key3", "value4")
                .withProperty("key4", "value4")
                .build();
        Queue updatedQueue = qSession.queueOperations().updateQueue(queue, queueUpdate);

        // assert
        assertThat(updatedQueue.purpose(), equalTo("New Purpose"));
        assertThat(updatedQueue.topic().get().value(), equalTo("newTopic"));

        QPropertySet expectedPS = QPropertySetBuilder.newSet().with("key3", "value3").with("key4", "value4").build();
        assertThat(updatedQueue.properties(), equalTo(expectedPS));


    }

    @Test
    public void test_deleting_queue() {
        // assemble
        String qName = Fixtures.rqName();
        QueueQuery query = QueueQueryBuilder.newQuery().withName(qName).build();
        QueueDefinition queueDef = QueueDefinitionBuilder.newDefinition().withName(qName).build();
        Queue queue = qSession.queueOperations().accessQueue(queueDef);

        // act
        Optional<Queue> queueExists = qSession.queueOperations().queryQueues(query).findFirst();

        qSession.queueOperations().deleteQueue(queue);

        Optional<Queue> queueNoLongerExists = qSession.queueOperations().queryQueues(query).findFirst();

        // assert
        assertThat(queueExists.isPresent(), equalTo(true));
        assertThat(queueNoLongerExists.isPresent(), equalTo(false));
    }

    @Test
    public void test_purging_queue() {
        // assemble
        String qName = Fixtures.rqName();
        QueueDefinition queueDef = QueueDefinitionBuilder.newDefinition().withName(qName).build();
        Queue queue = qSession.queueOperations().accessQueue(queueDef);
        Fixtures.writeNMessages(10, qSession, queue);

        Queue queueFull = qSession.queueOperations().getQueue(queue.id());

        // act
        qSession.queueOperations().purgeQueue(queue);

        Queue queueEmpty = qSession.queueOperations().getQueue(queue.id());

        // assert
        assertThat(queueFull, notNullValue());
        assertThat(queueFull.approxMsgCount(), equalTo(10L));

        assertThat(queueEmpty, notNullValue());
        assertThat(queueEmpty.approxMsgCount(), equalTo(0L));
    }
}
