package com.atlassian.psmq.backdoor.internal.bootstrap;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.psmq.api.QSessionFactory;
import com.atlassian.psmq.internal.visiblefortesting.QueueResetter;
import com.atlassian.sal.api.rdbms.TransactionalExecutorFactory;

/**
 * TODO: Document this class / interface here
 *
 * @since v0.x
 */
@Scanned
public class OsgiImports
{
    @ComponentImport
    EventPublisher eventPublisher;

    @ComponentImport
    QSessionFactory sessionFactory;

    @ComponentImport
    QueueResetter queueResetter;

    @ComponentImport
    TransactionalExecutorFactory executorFactory;
}
