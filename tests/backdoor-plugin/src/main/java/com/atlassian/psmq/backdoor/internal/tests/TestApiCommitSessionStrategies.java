package com.atlassian.psmq.backdoor.internal.tests;

import com.atlassian.psmq.api.QSession;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageConsumer;
import com.atlassian.psmq.api.message.QMessageProducer;
import com.atlassian.psmq.api.queue.Queue;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * These tests are all ignored and the SESSION based in PSMQ ability needs to be re-considered
 */
public class TestApiCommitSessionStrategies extends BaseApiTest {

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    @Ignore("This is not currently working because with new PKQDSL and SAL we are not longer allowed to call commit/rollback on managed connections")
    public void test_session_backout_happens() {
        psmqFixture.withConnection(connection -> {
            qSession = psmqFixture.sessionCommitSession(connection);

            // assemble
            String qName = Fixtures.rqName();

            Queue queue = qSession.queueOperations().accessQueue(Fixtures.qDefinition(qName));

            QMessageProducer producer = qSession.createProducer(queue);
            producer.writeMessage(Fixtures.basicMsg("Some uncommitted message 1"));
            producer.writeMessage(Fixtures.basicMsg("Some uncommitted message 2"));
            producer.writeMessage(Fixtures.basicMsg("Some uncommitted message 3"));
            producer.writeMessage(Fixtures.basicMsg("Some uncommitted message 4"));

            QMessageConsumer consumer = qSession.createConsumer(queue);
            Optional<QMessage> uncommittedMsg1 = consumer.claimAndResolve();
            Optional<QMessage> uncommittedMsg2 = consumer.claimAndResolve();
            /// two are left in the chamber at this stage

            long uncommittedMessageCount = qSession.queueOperations().browser(queue).getMessageCount();

            // pre assert
            assertThat(uncommittedMsg1.isPresent(), equalTo(true));
            assertThat(uncommittedMsg2.isPresent(), equalTo(true));
            assertThat(uncommittedMessageCount, equalTo(2L));

            // act
            qSession.rollback();

            Optional<QMessage> rollbackedMsg3 = consumer.claimAndResolve();
            Optional<QMessage> rollbackedMsg4 = consumer.claimAndResolve();
            long rollbackMessageCount = qSession.queueOperations().browser(queue).getMessageCount();


            // assert
            assertThat(rollbackedMsg3.isPresent(), equalTo(false));
            assertThat(rollbackedMsg4.isPresent(), equalTo(false));
            assertThat(rollbackMessageCount, equalTo(0L));
        });

    }

    @Test
    @Ignore("This is not currently working because with new PKQDSL and SAL we are not longer allowed to call commit/rollback on managed connections")
    public void test_session_commit_happens() {
        psmqFixture.withConnection(connection -> {
            qSession = psmqFixture.sessionCommitSession(connection);
            // assemble
            String qName = Fixtures.rqName();

            Queue queue = qSession.queueOperations().accessQueue(Fixtures.qDefinition(qName));

            QMessageProducer producer = qSession.createProducer(queue);
            producer.writeMessage(Fixtures.basicMsg("Some message 1"));
            producer.writeMessage(Fixtures.basicMsg("Some message 2"));
            producer.writeMessage(Fixtures.basicMsg("Some message 3"));
            producer.writeMessage(Fixtures.basicMsg("Some message 4"));

            QMessageConsumer consumer = qSession.createConsumer(queue);
            Optional<QMessage> msg1 = consumer.claimAndResolve();
            Optional<QMessage> msg2 = consumer.claimAndResolve();
            /// two are left in the chamber at this stage

            long uncommittedMessageCount = qSession.queueOperations().browser(queue).getMessageCount();

            // pre assert
            assertThat(msg1.isPresent(), equalTo(true));
            assertThat(msg2.isPresent(), equalTo(true));
            assertThat(uncommittedMessageCount, equalTo(2L));

            // act
            qSession.commit();

            Optional<QMessage> msg3 = consumer.claimAndResolve();
            Optional<QMessage> msg4 = consumer.claimAndResolve();
            long commitMsgCount = qSession.queueOperations().browser(queue).getMessageCount();


            // assert
            assertThat(msg3.isPresent(), equalTo(true));
            assertThat(msg4.isPresent(), equalTo(true));
            assertThat(commitMsgCount, equalTo(0L));
        });
    }

    @Test
    @Ignore("This is not currently working because I don' yet understand why it will lock in the database")
    public void test_session_isolation_happens() {
        psmqFixture.withConnection(connection1 -> {
            QSession session1 = psmqFixture.sessionCommitSession(connection1);

            psmqFixture.withConnection(connection2 -> {
                // assemble
                String qName = Fixtures.rqName();
                Queue queueCommon = psmqFixture.autoCommitSession().queueOperations().accessQueue(Fixtures.qDefinition(qName));

                QSession session2 = psmqFixture.sessionCommitSession(connection2);

                QMessageProducer producer1 = session1.createProducer(queueCommon);
                producer1.writeMessage(Fixtures.basicMsg("Some message 1"));
                producer1.writeMessage(Fixtures.basicMsg("Some message 2"));
                producer1.writeMessage(Fixtures.basicMsg("Some message 3"));

                QMessageProducer producer2 = session2.createProducer(queueCommon);
                producer2.writeMessage(Fixtures.basicMsg("Some other message 1"));

                // act
                QMessageConsumer consumer2 = session2.createConsumer(queueCommon);
                Optional<QMessage> otherMsg1 = consumer2.claimAndResolve();
                Optional<QMessage> otherMsg2 = consumer2.claimAndResolve();
                long sessionCount2 = session2.queueOperations().browser(queueCommon).getMessageCount();

                // assert
                // the 2nd session should not see work from the 1st session and only itself
                assertThat(otherMsg1.isPresent(), equalTo(true));
                assertThat(otherMsg2.isPresent(), equalTo(false));
                assertThat(sessionCount2, equalTo(0L));

            });
        });
    }


}