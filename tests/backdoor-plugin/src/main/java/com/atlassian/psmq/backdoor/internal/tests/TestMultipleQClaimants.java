package com.atlassian.psmq.backdoor.internal.tests;

import com.atlassian.psmq.api.QSession;
import com.atlassian.psmq.api.message.QClaimant;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageBuilder;
import com.atlassian.psmq.api.message.QMessageConsumer;
import com.atlassian.psmq.api.message.QMessageProducer;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueDefinition;
import com.atlassian.psmq.api.queue.QueueDefinitionBuilder;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * A test of multiple threads trying to claim a queue
 *
 * @since v0.x
 */
public class TestMultipleQClaimants extends BaseApiTest {

    static void say(String msg) {
        long l = TimeUnit.MICROSECONDS.convert(System.nanoTime(), TimeUnit.NANOSECONDS);
        System.out.println(String.format("%d %s - %s", l, Thread.currentThread().getName(), msg));
    }

    @Test
    public void test_multi_threaded_claimants() throws Exception {

        int NUM_MSGS_TO_WRITE = 100;
        int NUM_READERS = 3;
        String QUEUE_NAME = "multipleClaimantsQ";

        ExecutorService readExecutorService = Executors.newFixedThreadPool(NUM_READERS);

        Runnable onWrite = () -> {
            CountDownLatch beforeLatch = new CountDownLatch(NUM_READERS);
            CountDownLatch afterLatch = new CountDownLatch(NUM_READERS);
            List<Future<ReaderOfQueue>> reads = new ArrayList<>();
            for (int i = 0; i < NUM_READERS; i++) {
                ReaderOfQueue readerOfQueue = new ReaderOfQueue(psmqFixture, QUEUE_NAME, beforeLatch, afterLatch);
                reads.add(readExecutorService.submit(readerOfQueue));
            }

            int exclusiveCount = 0;
            int deniedCount = 0;
            // wait for all the reads to complete
            for (Future<ReaderOfQueue> read : reads) {
                try {
                    ReaderOfQueue readerOfQueue = read.get();
                    exclusiveCount += readerOfQueue.getExclusiveCount();
                    deniedCount += readerOfQueue.getDeniedCount();
                } catch (Exception e) {
                    say("Unable to wait for read thread : " + e);
                    throw new RuntimeException(e);
                }
            }

            // only 1 should have got access!
            assertThat("Only 1 reader should have got exclusive access", exclusiveCount, equalTo(1));
            assertThat("All other readers should have been denied", deniedCount, equalTo(NUM_READERS - 1));
        };

        ExecutorService writeExecutorService = Executors.newFixedThreadPool(1);
        Future<?> writeFuture = writeExecutorService.submit(new WriterToQueue(psmqFixture, NUM_MSGS_TO_WRITE, QUEUE_NAME, onWrite));

        // wait for writer to finish
        say("waiting for writer to finish");
        writeFuture.get();
    }

    static class ReaderOfQueue implements Callable<ReaderOfQueue> {
        private final Fixtures.Fixture psmqFixture;
        private final String queueName;
        private final AtomicInteger exclusiveCount = new AtomicInteger();
        private final AtomicInteger deniedCount = new AtomicInteger();
        private final CountDownLatch beforeLatch;
        private final CountDownLatch afterLatch;


        ReaderOfQueue(Fixtures.Fixture psmqFixture, String queueName, CountDownLatch beforeLatch, CountDownLatch afterLatch) {
            this.psmqFixture = psmqFixture;
            this.queueName = queueName;
            this.beforeLatch = beforeLatch;
            this.afterLatch = afterLatch;
        }

        @Override
        public ReaderOfQueue call() throws Exception {
            QSession qSession = psmqFixture.autoCommitSessionWithClaimant(
                    QClaimant.claimant("readerClaimant"), 60 * 1000
            );

            QueueDefinition queueDefinition = QueueDefinitionBuilder.newDefinition().withName(queueName).build();
            try {
                readWithExclusiveAccess(qSession, queueDefinition);
            } catch (InterruptedException e) {
                say("Unable to await count down latch : " + e);
                throw new RuntimeException(e);
            }
            qSession.close();
            return this;
        }

        private void readWithExclusiveAccess(QSession qSession, QueueDefinition queueDefinition) throws InterruptedException {
            say("reader started..counting down");
            beforeLatch.countDown();
            beforeLatch.await();
            say("reader asking for exclusive access");

            //
            // spin hard really trying to get access - our code should resist!
            long then = System.currentTimeMillis();
            int attempts = 0;
            Optional<Queue> queue = Optional.empty();
            while (!timeHasElapsed(then, 100, TimeUnit.MILLISECONDS)) {
                queue = qSession.queueOperations().exclusiveAccessQueue(queueDefinition);
                if (queue.isPresent()) {
                    break;
                }
                attempts += 1;
            }

            if (queue.isPresent()) {
                exclusiveCount.incrementAndGet();
                say("got exclusive access after " + attempts + " attempts");

                QMessageConsumer consumer = qSession.createConsumer(queue.get());
                Optional<QMessage> msg = consumer.claimAndResolve();
                if (msg.isPresent()) {
                    say("read message " + msg.get().buffer().asString());
                } else {
                    say("Well that is weird - there is no message.  Did some one beat us to it?");
                }

                // we are done
                afterLatch.countDown();

                // winner waits for all the others to lose.   This induces contention on the queue
                // otherwise they could finish their work, release the queue and others would get in.
                // we are testing for contention here- also MT tests are hard!
                afterLatch.await();
                qSession.queueOperations().releaseQueue(queue.get());
            } else {
                deniedCount.incrementAndGet();
                say("didnt get exclusive access after " + attempts + " attempts");

                // we are done
                afterLatch.countDown();
            }
        }

        private boolean timeHasElapsed(long then, int t, TimeUnit srcUnits) {
            long now = System.currentTimeMillis();
            long elapsed = TimeUnit.MILLISECONDS.convert(t, srcUnits);
            return (now - elapsed) > then;
        }

        public int getExclusiveCount() {
            return exclusiveCount.get();
        }

        public int getDeniedCount() {
            return deniedCount.get();
        }
    }

    static class WriterToQueue implements Runnable {
        private final Fixtures.Fixture psmqFixture;
        private final int numMsgs;
        private final String queueName;
        private final Runnable onWrite;

        WriterToQueue(Fixtures.Fixture psmqFixture, int numMsgs, String queueName, Runnable onWrite) {
            this.psmqFixture = psmqFixture;
            this.numMsgs = numMsgs;
            this.queueName = queueName;
            this.onWrite = onWrite;
        }

        public void run() {
            QSession qSession = psmqFixture.autoCommitSession();

            QueueDefinition queueDefinition = QueueDefinitionBuilder.newDefinition().withName(queueName).build();
            Queue queue = qSession.queueOperations().accessQueue(queueDefinition);
            QMessageProducer producer = qSession.createProducer(queue);
            for (int i = 0; i < numMsgs; i++) {
                QMessage msg = QMessageBuilder.newMsg().newBuffer().append(String.format("msg-%d", i)).buildBuffer().build();
                say("\n\nWriting message # " + i + "\n\n");
                producer.writeMessage(msg);
                //
                // say we have written something.
                onWrite.run();
            }
        }
    }

}
