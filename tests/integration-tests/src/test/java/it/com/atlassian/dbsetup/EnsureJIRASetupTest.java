package it.com.atlassian.dbsetup;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.SimpleJiraSetup;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.webdriver.testing.annotation.TestedProductClass;
import com.atlassian.webdriver.testing.annotation.WindowSize;
import com.atlassian.webdriver.testing.runner.ProductContextRunner;
import com.google.inject.Inject;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertTrue;

/**
 * Test that runs through the JIRA setup screens
 */
@TestedProductClass(JiraTestedProduct.class)
@RunWith(ProductContextRunner.class)
@WindowSize(width = 1024, height = 1000)
public class EnsureJIRASetupTest {
    @Inject
    private SimpleJiraSetup simpleJiraSetup;

    @Inject
    private Backdoor backdoor;

    @Test(timeout = 1_200_000L)
    public void dummyTestToRestoreData() {
        boolean isSetup = backdoor.dataImport().isSetUp();
            if (!isSetup) {
                simpleJiraSetup.performSetUp();
                isSetup = backdoor.dataImport().isSetUp();
            }
        assertTrue("The JIRA instance is not setup", isSetup);
    }

}
